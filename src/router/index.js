import Vue from 'vue'
import Router from 'vue-router'
import PreviewList from '@/components/PreviewList'
import Contract from '@/components/Contract'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'PreviewList',
      component: PreviewList
    },
    {
      path: '/contract',
      name: 'Contract',
      component: Contract,
      props: (route) => ({ address: route.query.address })
    }
  ]
})
