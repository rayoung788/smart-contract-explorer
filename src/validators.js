const numbersOnly = text => {
  return /^\d+$/.test(text) || 'This input needs to be a number.'
}

const validAddress = text => {
  return window.web3.utils.isAddress(text) || 'Invalid Address Format.'
}

const validCurrency = text => {
  return /^\d+(\.\d+)?$/.test(text) || 'Invalid Currency Format.'
}

export default { numbersOnly, validAddress, validCurrency }
