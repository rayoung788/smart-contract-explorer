import axios from 'axios'
const host = '/' // http://localhost:3002
// create contract server-side endpoint interface
// takes contract attributes as input
const createContract = async params => {
  const result = await axios.post(`${host}contract`, params)
  return result.data
}
// edit contract server-side endpoint interface
// takes contract attributes as input
const editContract = async params => {
  const result = await axios.put(`${host}contract`, params)
  return result.data
}
// delete contract server-side endpoint interface
// takes the address of the contract as input
const deleteContract = async address => {
  const result = await axios.delete(`${host}contract/${address}`)
  return result.data
}
// get all contracts server-side endpoint interface
const allContracts = async () => {
  const result = await axios.get(`${host}contract`)
  return result.data
}
// get contract details server-side endpoint interface
// takes the address of the contract as input
const getContract = async address => {
  const result = await axios.get(`${host}contract/${address}`)
  return result.data
}

export default {
  createContract,
  allContracts,
  getContract,
  editContract,
  deleteContract
}
