const mongoose = require('mongoose')
const Schema = mongoose.Schema

const contractSchema = new Schema({
  // name of the contract (doesnt have to correspond to the abi)
  name: String,
  // address of the contract on the blockchain
  address: { type: String, required: true, unique: true },
  // abi specification for contract interaction including methods, inputs, outputs, and events
  abi: { type: String, required: true },
  // high level description of what the contract does
  description: String
})

const Contract = mongoose.model('Contract', contractSchema)

module.exports = Contract
