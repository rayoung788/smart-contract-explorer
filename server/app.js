const express = require('express')
const morgan = require('morgan')
const mongoose = require('mongoose')
// file with environment variable configurations
const env = require('./env')
const path = require('path')
const contractRouter = require('./routers/contract')
// HTTP server
const app = express()
// setup the mongodb connection
mongoose.connect('mongodb://mongo:27017') // 192.168.99.100:27017
// point to the static files folder (built client files)
app.use(express.static('static'))
// logging
app.use(morgan('tiny'))
 // CRUD contract routes
app.use('/contract', contractRouter)
// client code entrypoint
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/index.html'))
})

app.listen(env.port, env.host, () =>
  console.log(`Gateway app listening on http://${env.host}:${env.port}/`)
)
