const express = require('express')
var cors = require('cors')
const Web3 = require('web3')
// mongoose ORM model
const Contract = require('./../models/contract')
// hard-coded web3 to point to our private testnet
// TODO: make this configurable
const web3 = new Web3(new Web3.providers.HttpProvider('http://23.24.168.98:8545'))
// needed to read post & put request data
const bodyParser = require('body-parser').json()
// router to be exported later
const router = express.Router()
// cors for development; turn off in production
router.use(cors())
// get information for a specfic contract address
// TODO: implement error handling
router.get('/:address', async (req, res) => {
  const address = req.params.address
  const contract = await Contract.findOne({ address })
  res.json({ contract })
})
// returns all contracts (string:name, string:address, string:descritpion)
// sorted by name
router.get('/', async (req, res) => {
  const contract = await Contract.find().sort('name').select('name address description')
  res.json({ contract })
})
// creates a new contract listing
router.post('/', bodyParser, async (req, res) => {
  let { address, name, abi, description } = req.body
  abi = JSON.stringify(abi)
  description = description || ''
  // check to see if the address is valid
  const code = await web3.eth.getCode(address)
  let result = 'No code at that address.'
  if (code !== '0x') {
    // TODO: error handling
    await Contract.create({ address, name, abi, description })
    result = 'Success!'
  }
  res.json({ result })
})
// updates an already existing contract
router.put('/', bodyParser, async (req, res) => {
  let { address, name, abi, description } = req.body
  abi = JSON.stringify(abi)
  description = description || ''
  let result = 'Success!'
  // TODO: error handling
  await Contract.findOneAndUpdate({ address }, { address, name, abi, description })
  res.json({ result })
})
// delete information for a specfic contract address
router.delete('/:address', bodyParser, async (req, res) => {
  const address = req.params.address
  let result = 'Success!'
  // TODO: error handling
  await Contract.findOneAndRemove({ address })
  res.json({ result })
})

module.exports = router
