# Smart Contract Explorer

> User interface to interact with arbitrary contracts hosted on the Cuvia Labs Private Ethereum Network. Heaviliy influenced by Parity's contract viewer UI.
> 
> Currently supports the following functionality: create, read, update, delete contract info (stored on a mongodb instance). Also provides an interface to execute smart contract methods on the private net blockchain.
> 
> Demo here: http://23.24.168.98:3002/ 
> 
> To view the demo: point metamask to our private network in the network settings: http://23.24.168.98:8545
> 
> Install Metamask here: https://metamask.io/

## Client Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
# requires the server running if standalone
# don't forget to change the host endpoint in ./src/api.js to point to the server! (or leave alone if building for prod)
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

## Server Build Setup
Requires Docker: https://www.docker.com/community-edition#/download
``` bash
# navigate to server folder
cd ./server

# starts mongodb instance at localhost:27017
# starts server at localhost:3002
# built client side files are included; no need to build client using the above instructions to run the app if you aren't making changes
docker-compose up -d
```
